package demo.zhaojie.activitydemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;

public class A extends AppCompatActivity implements View.OnClickListener{
    private final static String TAG = "activityA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_a);
        findViewById(R.id.aa).setOnClickListener(this);
        findViewById(R.id.ab).setOnClickListener(this);
        findViewById(R.id.ac).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        startActivity(new Intent(this, B.class));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "onNewIntent");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.aa:
                startActivity(new Intent(this, A.class));
                break;
            case R.id.ab:
                startActivity(new Intent(this, B.class));
                break;
            case R.id.ac:
                startActivity(new Intent(this, C.class));
        }
    }
}
