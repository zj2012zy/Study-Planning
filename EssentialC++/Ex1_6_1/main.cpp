#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int ival;
    int sum;
    vector<int> ivec;

    while(cin >> ival)
        ivec.push_back(ival);

    sum = 0;
    for(vector<int>::iterator i = ivec.begin(); i != ivec.end(); ++i)
        sum += *i;

    int average = sum/ivec.size();
    cout << "sum of " << ivec.size()
        << "\nelements: " << sum
        << "\nAverage: " << average << endl;
    return 0;
}
