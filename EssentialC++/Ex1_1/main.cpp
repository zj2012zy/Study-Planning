#include <iostream>
#include <string>

using namespace std;

int main()
{
    string first_name;
    cout << "Please Enter your first name:";
    cin >> first_name;
    cout << endl << "Hello, " << first_name << "...and good bye!" << endl;
    return 0;
}
