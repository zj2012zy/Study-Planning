#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

int main()
{
    ifstream in_file("./in.txt");
    if (!in_file)
    {
        cerr << "unable to open input file\n";
        return -1;
    }
    ofstream out_file("./out.txt");
    if (!out_file)
    {
        cerr << "unable to open out file\n";
        return -2;
    }

    string word;
    vector<string> text;
    while(in_file >> word)
        text.push_back(word);

    cout << "Unsorted text: ";
    for(vector<string>::iterator ix = text.begin(); ix != text.end(); ++ix)
        cout << *ix << " ";
    cout << endl;

    sort(text.begin(), text.end());
    cout << "Sorted text: ";
    for(vector<string>::iterator ix = text.begin(); ix != text.end(); ++ix)
    {
        cout << *ix << " ";
        out_file << *ix << " ";
    }

    cout << endl;
    out_file << endl;

    return 0;
}
