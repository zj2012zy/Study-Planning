#include <iostream>
#include <string>

using namespace std;

int main()
{
    string username;
    cout << "input your name:";
    cin >> username;

    switch (username.size())
    {
    case 0:
        cout << "hi, user with no name\n";
        break;
    case 1:
        cout << "A 1 character name ?"
            << "Ok, hi"
            << username << "\n";
        break;
    default:
        cout << "Hello" << username << endl;
        break;
    }
    return 0;
}
