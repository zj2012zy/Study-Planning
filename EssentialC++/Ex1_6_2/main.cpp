#include <iostream>

using namespace std;

int main()
{
    int ival, icnt = 0;
    const int array_size = 128;
    int ia[array_size];

    while(cin >> ival && icnt < array_size)
        ia[icnt++] = ival;

    int sum = 0;
    for(int ix = 0; ix < icnt; ++ix)
        sum += ia[ix];

    int average = sum / icnt;
    cout << "sum of " << icnt
        << " elements: " << sum
        << " average: " << average << endl;
    return 0;
}
