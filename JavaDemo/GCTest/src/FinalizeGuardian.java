class Parent2{
	private final Object finalizeGuardian = new Object() {
		protected void finalize() throws Throwable {
			System.out.println("在此执行父类终结方法中的逻辑");
		};
	};
}

class Son2 extends Parent2{
	@Override
	protected void finalize() throws Throwable {
		System.out.println(getClass().getName() + " finalize start");
		int i = 5 / 0;
		super.finalize();
	}
}

public class FinalizeGuardian {

	public static void main(String[] args) {
		new Son2();
		System.gc();
	}

}
