import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ManualClear {

	public static void main(String[] args) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream("./src/ManualClear.java");
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			return;
		}

		try {
			byte[] bbuf = new byte[1024];
			int hasRead = 0;
			try {
				while ((hasRead = fileInputStream.read(bbuf)) > 0) {
					System.out.println(new String(bbuf, 0, hasRead));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} finally {
			try {
				fileInputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
