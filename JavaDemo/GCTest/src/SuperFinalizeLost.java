class Parent{
	@Override
	protected void finalize() throws Throwable {
		System.out.println(getClass().getName() + " finalize start");
	}
}

class Son extends Parent{
	@Override
	protected void finalize() throws Throwable {
		System.out.println(getClass().getName() + " finalize start");
		int i = 5 / 0;
		super.finalize();
	}
}
public class SuperFinalizeLost {

	public static void main(String[] args) {
		new Son();
		System.gc();
	}

}
