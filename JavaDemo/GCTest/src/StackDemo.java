import java.util.Arrays;
import java.util.EmptyStackException;

class Stack{
	private Object[] elements;
	private int size;
	private static final int DEFAULT_INITAL_CAPACITY = 16;
	
	public Stack() {
		elements = new Object[DEFAULT_INITAL_CAPACITY];
	}
	
	public void push(Object e){
		ensureCapacity();
		elements[size++] = e;
	}
	
	public Object pop() {
		if (size == 0) {
			throw new EmptyStackException();
		}
		
		return elements[--size];
	}
	
	private void ensureCapacity() {
		if (elements.length == size) {
			elements = Arrays.copyOf(elements, 2 * size + 1);
		}
	}
}

public class StackDemo {
	
	public static void main(String[] args) {
		Stack stack = new Stack();
		
		for (int i = 0; i < 10000; i++) {
			stack.push(new Object());
		}
		
		for(int i = 0; i < 10000; i++) {
			stack.pop();
		}
	}

}
