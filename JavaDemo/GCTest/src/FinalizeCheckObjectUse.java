class Book {
	boolean checkout = false;
	public Book(boolean checkout) {
		this.checkout = checkout;
	}
	
	public void checkin(){
		checkout = false;
	}
	
	@Override
	protected void finalize() throws Throwable {
		if (checkout) {
			System.out.println("Error: check out");
		}
	}
}

public class FinalizeCheckObjectUse {

	public static void main(String[] args) {
		new Book(true);
		System.gc();
	}

}
