import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledThreadPool {
	static class Task implements Runnable {
		@Override
		public void run() {
			System.out.println("time " + System.currentTimeMillis()  + " " + Thread.currentThread().getName() + " AllStackTraces map size: "
					+ Thread.currentThread().getAllStackTraces().size());
		}
	}

	public static void main(String[] args) {
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(3);
		
		scheduledExecutorService.schedule(new Task(), 3, TimeUnit.SECONDS);
		
		scheduledExecutorService.scheduleAtFixedRate(new Task(), 3, 5, TimeUnit.SECONDS);
	
		try {
			Thread.sleep(30 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		scheduledExecutorService.shutdown();
	}

}
