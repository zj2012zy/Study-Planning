
	public class JVMCrashTest {
		static class MyThread extends Thread {
			@Override
			public void run() {
				System.out.println(getName());
				try {
					sleep(10 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	
		public static void main(String[] args) {
			for(int i = 0; i < 100000; i++) {
				new MyThread().start();
			}
		}
	
	}
