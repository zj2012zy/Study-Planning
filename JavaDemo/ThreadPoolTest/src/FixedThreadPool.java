import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPool {
	static class Task implements Runnable {
		@Override
		public void run() {
			System.out.println(this + " " + Thread.currentThread().getName() + " AllStackTraces map size: "
					+ Thread.currentThread().getAllStackTraces().size());
		}
	}

	public static void main(String[] args) {
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);

		// 先添加三个任务到线程池
		for (int i = 0; i < 5; i++) {
			fixedThreadPool.execute(new Task());
		}

		// 等三个线程执行完成后，再次添加三个任务到线程池
		try {
			Thread.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < 3; i++) {
			fixedThreadPool.execute(new Task());
		}
	}

}
