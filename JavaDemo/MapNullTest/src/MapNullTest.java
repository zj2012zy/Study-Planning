import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;

public class MapNullTest {

	public static void main(String[] args) {
		//1. HashMap null test
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put(null, null);
		hashMap.put(null, null);
		hashMap.put("a", null);
		hashMap.put("b", null);
		System.out.println("hashMap: " + hashMap);
		
		//2. HashTable null test
		Hashtable<String, String> hashTable = new Hashtable();
		try {
			hashTable.put(null, "a");	
		} catch (NullPointerException e) {
			System.out.println("hashTable put (null, a) NullPointerException");
		}
		
		try {
			hashTable.put("a", null);
		} catch (Exception e) {
			System.out.println("hashTable put (a, null) NullPointerException");
		}
		System.out.println("hashTable: " + hashTable);
		
		//3. TreeMap null test
		TreeMap<String, String> treeMap = new TreeMap<>();
		try {
			treeMap.put(null, "a");	
		} catch (NullPointerException e) {
			System.out.println("treeMap put (null, a) NullPointException");
		}
		
		treeMap.put("a", null);
		treeMap.put("b", null);
		System.out.println("treeMap: " + treeMap);
	}

}
