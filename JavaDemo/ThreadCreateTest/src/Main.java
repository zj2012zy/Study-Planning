import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class Main {

	public static void main(String[] args) {
		//方法一：继承Thread
		int i = 0;
//		for(; i < 100; i++){
//			System.out.println(Thread.currentThread().getName() + " " + i);
//			if (i == 5) {
//				ThreadExtendsThread threadExtendsThread = new ThreadExtendsThread();
//				threadExtendsThread.start();
//			}
//		}
		
		//方法二：实现Runnable
//		for(i = 0; i < 100; i++){
//			System.out.println(Thread.currentThread().getName() + " " + i);
//			if (i == 5) {
//				Runnable runnable = new ThreadImplementsRunnable();
//				new Thread(runnable).start();
//				new Thread(runnable).start();
//			}
//		}

		//方法三：实现Callable接口
		Callable<Integer> callable = new ThreadImplementsCallable();
		FutureTask<Integer> futureTask = new FutureTask<>(callable);
		for(i = 0; i < 100; i++){
			System.out.println(Thread.currentThread().getName() + " " + i);
			if (i == 5) {
				new Thread(futureTask).start();
				new Thread(futureTask).start();
				try {
					Thread.sleep(1);	
				} catch (Exception e) {
				}
				
			}
		}
		try {
			System.out.println("futureTask ruturn: " + futureTask.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
