
public class ParamTransferTest {
	
	public static void swap(int a, int b) {
		int temp = a;
		a = b;
		b = temp;
		System.out.println("swap() a = " + a + " b = " + b);
	}
	
	public static void swap(DataWrap dataWrap) {
		int temp = dataWrap.a;
		dataWrap.a = dataWrap.b;
		dataWrap.b = temp;
		
		System.out.println("swap() datawrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
	}
	
	public static void newObject(DataWrap dataWrap) {
		dataWrap = new DataWrap();
		
		System.out.println("newObject() datawrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
	}
	
	public static void destoryObject(DataWrap dataWrap) {
		dataWrap = null;
		
		System.out.println("destoryObject() datawrap = " + dataWrap);
	}
	
	public static void swapObject(DataWrap a, DataWrap b){
		System.out.println("in swap before a = " + a + " b = " + b);
		DataWrap temp = a;
		a = b;
		b = temp;
		System.out.println("in swap after a = " + a + " b = " + b);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1.PrimitiveTransferTest
		int a = 6;
		int b = 9;
		
		System.out.println("before swap, a = " + a + " b = " + b);
		swap(a, b);
		System.out.println("after swap, a = " + a + " b = " + b);
		
		//2.RefTransferTest
		DataWrap dataWrap = new DataWrap();
		dataWrap.a = a;
		dataWrap.b = b;
		
		System.out.println("before swap, dataWrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
		swap(dataWrap);
		System.out.println("after swap, dataWrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
		
		//3.in method create new object
		System.out.println("before newObject, dataWrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
		newObject(dataWrap);
		System.out.println("after newObject, dataWrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
		
		//4.in method destory object
		System.out.println("before destoryObject, dataWrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
		destoryObject(dataWrap);
		System.out.println("after destoryObject, dataWrap = " + dataWrap + " Field a = " + dataWrap.a + " Field b = " + dataWrap.b);
		
		//5.swap object
		DataWrap dataWrapA = new DataWrap();
		DataWrap dataWrapB = new DataWrap();
		System.out.println("before swap, dataWrapA: " + dataWrapA + " dataWrapB = = " + dataWrapB);
		swapObject(dataWrapA, dataWrapB);
		System.out.println("after swap, dataWrapA: " + dataWrapA + " dataWrapB: " +dataWrapB);
	}
}
