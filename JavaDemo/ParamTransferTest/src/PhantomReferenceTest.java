import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class PhantomReferenceTest {

	public static void main(String[] args) {
		String str = new String("Hello World");
		ReferenceQueue referenceQueue = new ReferenceQueue<>();
		
		PhantomReference<String> phantomReference = new PhantomReference<String>(str, referenceQueue);
		
		str = null;
		
		System.out.println(phantomReference.get());
		
		System.gc();
		System.runFinalization();
		
		System.out.println(referenceQueue.poll() == phantomReference);
	}

}
