public class SyncMethod {
	
	static class DataWrap{
		int i;
		
		public synchronized void valueGrow(){
			i++;
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(Thread.currentThread().getName() + " " + i);
		}
	}
	
	static class SyncMethodThread extends Thread {
		DataWrap dataWrap;
		
		public SyncMethodThread(DataWrap dataWrap){
			this.dataWrap = dataWrap;
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				dataWrap.valueGrow();	
			}
		}
	}
	
	public static void main(String[] args) {
		//实现顺序增长并输出Datawrap中的i
		
		DataWrap dataWrap = new DataWrap();
		
		new SyncMethodThread(dataWrap).start();
		new SyncMethodThread(dataWrap).start();
		new SyncMethodThread(dataWrap).start();
	}

}
