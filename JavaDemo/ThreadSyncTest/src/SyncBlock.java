
public class SyncBlock {
	static class DataWrap {
		int i;
	}
	
	static class SyncBlockThread extends Thread {
		private DataWrap date;
		
		public SyncBlockThread(DataWrap dataWrap) {
			this.date = dataWrap;
		}
		
		@Override
		public void run() {
			
			for (int i = 0; i < 10; i++) {
				synchronized (date) {
					date.i++;
					try {
						sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(getName() + " " + date.i);
				}
			}
		}
	}
	
	public static void main(String[] args) {
		//多线程实现变量i依次加一输出
		DataWrap dataWrap = new DataWrap();
		
		new SyncBlockThread(dataWrap).start();
		new SyncBlockThread(dataWrap).start();
		new SyncBlockThread(dataWrap).start();
	}

}
