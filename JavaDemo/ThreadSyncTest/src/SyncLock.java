import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SyncLock {

	static class DataWrap{
		Lock lock = new ReentrantLock();
		int i;
		
		public void valueGrow(){
			lock.lock();
			try {
				i++;
				
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(Thread.currentThread().getName() + " " + i);	
			} finally {
				lock.unlock();
			}
			
		}
	}
	
	static class SyncLockThread extends Thread {
		DataWrap dataWrap;
		
		public SyncLockThread(DataWrap dataWrap){
			this.dataWrap = dataWrap;
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 10; i++) {
				dataWrap.valueGrow();	
			}
		}
	}
	
	public static void main(String[] args) {
		//实现顺序增长并输出Datawrap中的i
		
		DataWrap dataWrap = new DataWrap();
				
		new SyncLockThread(dataWrap).start();
		new SyncLockThread(dataWrap).start();
		new SyncLockThread(dataWrap).start();
	}

}
