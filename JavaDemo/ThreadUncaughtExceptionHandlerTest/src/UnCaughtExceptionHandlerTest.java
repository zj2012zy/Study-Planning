public class UnCaughtExceptionHandlerTest {
	static class UnCaughtExceptionHandler implements Thread.UncaughtExceptionHandler{

		@Override
		public void uncaughtException(Thread t, Throwable e) {
			System.out.println(t + " 异常：" + e);
		}
		
	}
	
	public static void main(String[] args) {
		Thread.currentThread().setUncaughtExceptionHandler(new UnCaughtExceptionHandler());
		
		int a = 5 / 0;
		System.out.println("normal over!");
	}

}
