
public class DaemonThreadTest extends Thread{
	
	public DaemonThreadTest(String name){
		super(name);
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 1000; i++) {
			System.out.println(getName() + " " + i);
		}
	}
	
	public static void main(String[] args) {
		DaemonThreadTest daemonThreadTest = new DaemonThreadTest("DaemonThread");
		daemonThreadTest.setDaemon(true);
		daemonThreadTest.start();
		for (int i = 0; i < 100; i++) {
			System.out.println(Thread.currentThread().getName() + " " + i);
		}
	}
}
