
public class YieldTest extends Thread {
	
	public YieldTest(String name) {
		super(name);
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			System.out.println(getName() + " " + i);
			if (i== 20) {
				Thread.yield();
			}
		}
	}
	
	public static void main(String[] args) {
		YieldTest yieldTest1 = new YieldTest("High");
		yieldTest1.setPriority(MAX_PRIORITY);
		yieldTest1.start();
		
		YieldTest yieldTest2 = new YieldTest("Low");
		yieldTest2.setPriority(MIN_PRIORITY);
		yieldTest2.start();
	}

}
