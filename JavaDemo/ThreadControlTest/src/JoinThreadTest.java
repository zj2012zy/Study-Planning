public class JoinThreadTest extends Thread {
	
	public JoinThreadTest(String name){
		super(name);
	}
	
	@Override
	public void run() {
		for(int i = 0; i < 100; i++){
			System.out.println(getName() + " " + i);
		}
	}
	
	public static void main(String[] args) {
		for(int i = 0; i < 1000; i++){
			if (i== 20) {
				JoinThreadTest joinThreadTest = new JoinThreadTest("JoinThread");
				joinThreadTest.start();
				try {
					joinThreadTest.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			System.out.println(Thread.currentThread().getName() + " " + i);
		}
	}

}
