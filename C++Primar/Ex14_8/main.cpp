#include <iostream>
#include <string>

using namespace std;

class Employee
{
private:
    string m_name;
    int m_i;
public:
    Employee(string name, int i):m_name(name), m_i(i){}
    friend ostream &operator<<(ostream &out, const Employee &emp);
};

ostream &operator<<(ostream &out, const Employee &emp)
{
    out << emp.m_name << '\t' << emp.m_i;

    return out;
}
int main()
{
    Employee emp("zhaojie", 10);
    cout << emp << endl;
    return 0;
}
