#include <iostream>
#include <vector>
#include <string>

using namespace std;

template <typename T>
void print(const T& t)
{
    for(typename T::size_type i = 0; i < t.size(); ++i)
        cout << t[i] << endl;
}

int main()
{
    vector<string> vecstr;
    vecstr.push_back("zhaojie");
    vecstr.push_back("love");
    vecstr.push_back("zhangying");
    vecstr.push_back("zhangying");
    print(vecstr);
    return 0;
}
