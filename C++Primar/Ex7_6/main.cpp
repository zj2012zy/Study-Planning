#include <iostream>

using namespace std;

void changevalue(int *p1, int *p2)
{
    int temp;
    temp = *p2;
    *p2 = *p1;
    *p1 = temp;
}
int main()
{
    int a = 2, b = 3;
    changevalue(&a, &b);
    cout << a << b << endl;
    return 0;
}
