#include <iostream>
#include <list>

using namespace std;

class foo
{
private:
    int i;
public:
    foo(int a)
    {
        i = a;
    }
    friend ostream& operator << (ostream & out, const foo &val);
};

ostream& operator << (ostream & out, const foo &val)
{
    out << val.i;
    return out;
}

int main()
{
    list<foo> foolist(10, 5);
    for (list<foo>::iterator i = foolist.begin(); i != foolist.end(); ++i)
        cout << (*i) << endl;
    return 0;
}
