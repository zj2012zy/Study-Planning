#include <iostream>

using namespace std;

class HasPtr
{
private:
    int *ptr;
    int m_i;
public:
    HasPtr(const int &p, int i):ptr(new int(p)), m_i(i)
    {

    }
    HasPtr(const HasPtr &orig):ptr(new int(*orig.ptr)), m_i(orig.m_i)
    {

    }
    ~HasPtr()
    {
        delete ptr;
    }
    HasPtr &operator= (const HasPtr* orig);

    int *get_ptr() const
    {
        return ptr;
    }
    int get_val() const
    {
        return m_i;
    }
    int get_ptr_val() const
    {
        return *ptr;
    }
    void set_ptr(int *p)
    {
        ptr = p;
    }
    void set_val(int i)
    {
        m_i = i;
    }
    void set_ptr_val(int i)
    {
        *ptr = i;
    }

};
int main()
{

    HasPtr hasptr(2, 2);
    hasptr.set_ptr_val(5);
    HasPtr hasptr2 = hasptr;
    cout << hasptr2.get_ptr_val() << endl;
    return 0;
}
