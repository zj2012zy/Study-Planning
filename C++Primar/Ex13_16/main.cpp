#include <iostream>
#include <string>
#include <set>

using namespace std;

class Folder;

class Message
{
private:
    string contents;
    set<Folder*> folders;

    void put_msg_in_folders(const set<Folder*>&);
    void rem_msg_from_folders();
public:
    Message(const string& str = ""):contents(str) {}
    Message(const Message& msg);
    Message& operator=(const Message& msg);
    ~Message();

    void save(Folder&);
    void remove(Folder&);
};
int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
