#include <iostream>

using namespace std;

int main()
{
    int i, j;
    cin >> i >> j;
    cout << ((i > j) ? i : j) << endl;
    return 0;
}
