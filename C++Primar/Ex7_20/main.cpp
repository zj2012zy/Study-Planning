#include <iostream>

using namespace std;

int factorial(int val)
{
    int result = 1;
    do
    {
        result *= val;
    }while((val--) > 1);

    return result;
}

int main()
{
    int i;
    cin >> i;
    cout << factorial(i) << endl;
    return 0;
}
