#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> vec;
    vec.push_back(5);
    vector<int>::reference val1 = *vec.begin();
    vector<int>::reference val2 = vec.front();
    vector<int>::reference val3 = vec[0];
    vector<int>::reference val4 = vec.at(0);

    cout << val1 << val2 << val3 << val4 << endl;
    return 0;
}
