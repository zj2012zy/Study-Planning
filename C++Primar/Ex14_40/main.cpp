#include <iostream>
#include <string>

using namespace std;

class Sale_Item
{
private:
    float units_sold;
    string m_isbn;

    friend ostream &operator<<(ostream &out, const Sale_Item &saleitem);
    friend istream &operator>>(istream &in, Sale_Item &saleitem);
public:
    Sale_Item(float sold = 0):units_sold(sold)
    {

    }

    Sale_Item &operator+=(const Sale_Item &saleitem);
    Sale_Item &operator=(string isbn);
    operator string()
    {
        return m_isbn;
    }
    operator float()
    {
        return units_sold;
    }
};

Sale_Item &Sale_Item::operator+=(const Sale_Item &saleitem)
{
    units_sold += saleitem.units_sold;
    return *this;
}

Sale_Item operator+(const Sale_Item &saleitem1, const Sale_Item &saleitem2)
{
    Sale_Item ret(saleitem1);
    ret += saleitem2;

    return ret;
}

ostream &operator<<(ostream &out, const Sale_Item& saleitem)
{
    out << saleitem.m_isbn << '\t' << saleitem.units_sold;
    return out;
}

Sale_Item &Sale_Item::operator=(string isbn)
{
    m_isbn = isbn;
    return *this;
}
int main()
{
    Sale_Item s1(5), s2(2.2);
    s1 = "100-23-43";

    cout << s1 + s2 << endl;
    cout << string(s1) << endl;
    cout << float(s2) << endl;
    return 0;
}
