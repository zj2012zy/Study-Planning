#include <iostream>
#include <stack>
#include <string>

using namespace std;

int main()
{
    stack<string> strstack;
    string str;
    while(cin >> str)
        strstack.push(str);

    while(!strstack.empty())
    {
        cout << strstack.top() << endl;
        strstack.pop();
    }
    return 0;
}
