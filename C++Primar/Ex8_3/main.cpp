#include <iostream>
#include <fstream>

using namespace std;


istream & read(istream & in)
{
    string val;
    istream::iostate old_state = in.rdstate();
    in.clear();
    while(in >> val)
    {
        cout << val;
    }
    in.clear(old_state);
    return in;
}
int main()
{
    string str;
    ifstream fin;
    fin.open("/home/zhaoj/code/C++Primar/Ex8_3/main.cpp");
    read(fin) >> str;
    fin.clear();
    fin.close();
    cout << str << endl;
    return 0;
}
