#include <iostream>
#include <list>
#include <algorithm>

using namespace std;

class GT_cls
{
private:
    int val;
public:
    GT_cls(int i = 0):val(i){}

    bool operator()(int i)
    {
        return i == val;
    }
};

int main()
{
    list<int> lisint;
    for(int i = 0; i < 10; ++i)
        lisint.push_back(i);
    replace_if(lisint.begin(), lisint.end(), GT_cls(3), 0);
    for (list<int>::iterator i = lisint.begin(); i != lisint.end(); ++i)
        cout << *i << endl;
    return 0;
}
