#include <iostream>
#include <utility>
#include <vector>
#include <string>

using namespace std;

int main()
{
    string str;
    int i;
    vector< pair<string, int> > pairvec;
    while(cin >> str >> i)
    {
        pairvec.push_back(make_pair(str, i));
    }

    for(vector< pair<string, int> >::iterator i = pairvec.begin(); i != pairvec.end(); ++i)
        cout << i->first << i->second << endl;
    return 0;
}
