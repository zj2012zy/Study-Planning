#include <iostream>
#include <map>
#include <string>
#include <utility>

using namespace std;

int main()
{
    map<string, int> word_count;
    string word;

    while(cin >> word)
    {
        pair< map<string, int>::iterator, bool> ret = word_count.insert(make_pair(word, 1));
        if (!ret.second)
            ++(ret.first->second);
    }

    for(map<string, int>::iterator i = word_count.begin(); i != word_count.end(); ++i)
        cout << i->first << " " << i->second << endl;
    return 0;
}
