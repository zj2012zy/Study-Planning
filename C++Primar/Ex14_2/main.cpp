#include <iostream>

using namespace std;
class Sale_Item
{
private:
    friend ostream &operator<<(ostream &out, const Sale_Item &saleitem);
    friend istream &operator>>(istream &in, Sale_Item &saleitem);
public:
    Sale_Item &operator+=(const Sale_Item &saleitem);
};

Sale_Item operator+(const Sale_Item &saleitem1, const Sale_Item &saleitem2);
int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
