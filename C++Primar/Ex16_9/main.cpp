#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

template <typename IterType, typename ValueType>
bool find(IterType beg, IterType en, ValueType value)
{
    while(beg++ != en)
    {
        if(*beg == value)
            return 1;
    }
    return 0;
}
int main()
{
    vector<int> vecint;
    for(int i = 0; i < 10; ++i)
        vecint.push_back(i);
    cout << find(vecint.begin(), vecint.end(), 0) << endl;

    list<string> lisstr;
    lisstr.push_back("");
    lisstr.push_back("zhao");
    lisstr.push_back("jie");
    lisstr.push_back("zhang");
    lisstr.push_back("ying");

    cout << find(lisstr.begin(), lisstr.end(), "zhao") << endl;
    lisstr.value_type;
    return 0;
}
