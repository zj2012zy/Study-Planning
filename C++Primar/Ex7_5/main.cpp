#include <iostream>

using namespace std;

int max(int i,const int *pi)
{
    return (i >= *pi)? i : *pi;
}
int main()
{
    int a = 2, b = 3;

    cout << max(a, &b) << endl;
    return 0;
}
