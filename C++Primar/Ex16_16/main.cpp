#include <iostream>
#include <string>

using namespace std;

template <typename T, size_t N>
void printvalues(T (&param)[N])
{
    for (size_t i = 0; i < N; ++i)
        cout << *(param + i) << " ";
    cout << endl;
}
int main()
{
    string strary[] = {"zhaojie", "love", "zhangying", "very", "much"};
    printvalues(strary);
    return 0;
}
