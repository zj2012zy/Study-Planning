#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int base, exponent;
    int result;

    cin >> base >> exponent;
    result = 1;
    for (int i = 0; i < exponent; i++)
    {
        result *= base;
    }
    cout << result << endl;
    return 0;
}
