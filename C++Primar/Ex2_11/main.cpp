#include <iostream>

using namespace std;

int main()
{
    int base, result, exponent, i;

    cin >> base >> exponent;
    result = 1;
    for (i = 0; i < exponent; i++)
        result *= base;
    cout << result << endl;
    return 0;
}
