#include <iostream>
#include <cstring>
#include <string>

using namespace std;

int main()
{
    const char *cstr1 = "hello";
    const char *cstr2 = "world";

    char *cresult = new char[strlen(cstr1) + strlen(cstr2) + 1];

    int i = 0;
    int j = 0;
    while(*(cstr1 + i))
    {
        *(cresult + j) = *(cstr1 + i);
        ++i;
        ++j;
    }
    i = 0;
    while(*(cstr2 + i))
    {
        *(cresult + j) = *(cstr2 + i);
        ++i;
        ++j;
    }
    *(cresult + j) = '\n';

    i = 0;
    while(*(cresult + i))
    {
        cout << *(cresult + i);
        ++i;
    }
    delete [] cresult;

    string str1 = "hello";
    string str2 = "world";

    string str3 = str1 + str2;
    cout << str3 << endl;
    return 0;
}
