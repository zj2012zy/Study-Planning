#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
    string words[] = {"haha", "hehe", "heihei"};

    vector<string> vec1(3);
    vector<string> vec2(words, words + (sizeof(words)/sizeof(words[0])));
    vector<string> vec3(vec2);
    vector<string> vec4(vec3.begin(), vec3.end());
    for(size_t i = 0; i < 3; ++i)
    {
        cout << vec1[i] << endl;
        cout << vec2[i] << endl;
        cout << vec3[i] << endl;
        cout << vec4[i] << endl;
    }
    return 0;
}
