#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class GT_cls
{
private:
    int bound;
public:
    GT_cls(int i = 0):bound(i)
    {

    }
    bool operator()(const int i)
    {
        return i > bound;
    }
};
int main()
{
    vector<int> vecint;
    for(int i = 0; i < 10; ++i)
        vecint.push_back(i);
    cout << *find_if(vecint.begin(), vecint.end(), GT_cls(10)) << endl;
    return 0;
}
