#include <iostream>

using namespace std;

int main()
{
    int val1, val2;
    int  *p1, *p2;
    val1 = 1;
    val2 = 2;

    p1 = &val1;
    p2 = &val2;

    p1 = p2;
    *p1 = 3;
    cout << *p1 << endl;
    return 0;
}
