#include <iostream>
#include <map>
#include <string>

using namespace std;

int main()
{
    map<string, int> word_count;
    string word;

    while(cin >> word)
        ++word_count[word];

    for(map<string, int>::iterator i = word_count.begin(); i != word_count.end(); ++i)
        cout << i->first << " " << i->second << endl;
    return 0;
}
