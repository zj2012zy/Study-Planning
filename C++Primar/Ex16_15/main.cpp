#include <iostream>
#include <string>

using namespace std;

template <typename T, size_t N>
size_t arraylength(T (&param)[N])
{
    return N;
}

int main()
{
    int intary[45];
    string strary[50];
    cout << arraylength(intary) << endl;
    cout << arraylength(strary) << endl;
    return 0;
}
