#include <iostream>

using namespace std;

void error(const string &, const int, const int);
void error(const string &);
void error(const string &, const char);
int main()
{
    int index = 0, upperBound = 5;
    char selectVal = 'Q';

    error("Subscript out of bounds: ", index, upperBound);
    error("Division by zero");
    error("Invalid selection", selectVal);
    return 0;
}

void error(const string &str, const int index, const int upperBound)
{
    cout << str << index << upperBound << endl;
}

void error(const string &str)
{
    cout << str << endl;
}

void error(const string &str, const char ch)
{
    cout << str << ch << endl;
}
