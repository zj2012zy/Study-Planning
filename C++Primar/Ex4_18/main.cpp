#include <iostream>
#include <cctype>

using namespace std;

int main()
{
    const size_td array_size = 5;
    int ary[array_size] = {1, 2, 3, 4, 5};

    for (int *pary = ary, *pend = ary + array_size; pary != pend; ++pary)
        *pary = 0;

    for (size_t i = 0; i < array_size; ++i)
        cout << ary[i] << endl;
    return 0;
}
