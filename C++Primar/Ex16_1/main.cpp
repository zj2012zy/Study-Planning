#include <iostream>

using namespace std;

template <typename T>
T abs(T t)
{
    if (t >= 0)
        return t;
    else
        return -t;
}

int main()
{
    cout << abs(-2) << endl;
    cout << abs(-0.1) << endl;
    return 0;
}
