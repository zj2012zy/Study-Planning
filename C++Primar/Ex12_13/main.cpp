#include <iostream>
#include <string>

using namespace std;

class Screen
{
public:
    typedef string::size_type index;

    Screen(string contents, index height, index width):m_contents(contents), m_height(height), m_width(width)
    {

    }

    char get() const
    {
        return m_contents[m_cursor];
    }

    char get(index r, index c) const;
    Screen &move(index r, index c);
    Screen &set(char ch);
    Screen &set(index r, index c, char ch);

    const Screen &display(ostream &out) const;
    Screen &display(ostream &out);
private:
    string m_contents;
    index m_cursor;
    index m_height, m_width;

    void do_dispaly(ostream &out) const;
};

char Screen::get(index r, index c) const
{
    index row = r * m_width;
    return m_contents[row + c];
}

Screen &Screen::move(index r, index c)
{
    m_cursor = r * m_width + c;
    return *this;
}

Screen &Screen::set(char ch)
{
    m_contents[m_cursor] = ch;
    return *this;
}

Screen &Screen::set(index r, index c, char ch)
{
    m_contents[r * m_width + c] = ch;
    return *this;
}

Screen &Screen::display(ostream &out)
{
    do_dispaly(out);
    return *this;
}

const Screen &Screen::display(ostream &out) const
{
    do_dispaly(out);
    return *this;
}

inline void Screen::do_dispaly(ostream &out) const
{
    out << m_contents;
}
int main()
{
    Screen screen("I LOVE YOU", 5, 5);
    cout << screen.get(1, 2) << endl;
    screen.move(1, 1).set('#').display(cout);
    return 0;
}

