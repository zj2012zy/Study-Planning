#include <iostream>
#include <list>
#include <deque>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
    list<string> lis;
    deque<string> deq;
    string str;
    while(cin >> str)
    {
        lis.push_back(str);
        deq.push_back(str);
    }

    list<string>::iterator ilis = find(lis.begin(), lis.end(), "zhaojie");
    if (ilis != lis.end())
        lis.erase(ilis);
    for(list<string>::iterator i = lis.begin(); i != lis.end(); ++i)
        cout << *i << endl;

    deque<string>::iterator ideq = find(deq.begin(), deq.end(), "zhangying");
    if( ideq != deq.end())
        deq.erase(ideq);
    for(deque<string>::iterator i = deq.begin(); i != deq.end(); ++i)
        cout << *i << endl;
    return 0;
}
