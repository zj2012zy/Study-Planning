#include <iostream>

using namespace std;

int main()
{
    int sum = 0;
    int j;
    for (int i = 50; i <= 100; i++)
        sum += i;
    cout << sum << endl;

    sum = 0;
    j = 50;
    while(j <= 100)
    {
        sum += j;
        j++;
    }
    cout << sum << endl;
    return 0;
}
