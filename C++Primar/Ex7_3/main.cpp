#include <iostream>

using namespace std;

int mi(int dishu, int xishu)
{
    int result = 1;
    while(xishu--)
    {
        result = dishu * dishu;
    }
    return result;
}

int jueduizhi(int i)
{
    if (i >= 0)
        return i;
    else
        return -i;
}
int main()
{
    cout << mi(0, 0) << endl;
    cout << jueduizhi(-2) << endl;
    return 0;
}
