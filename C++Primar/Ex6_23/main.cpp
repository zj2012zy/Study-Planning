#include <iostream>
#include <bitset>
#include <stdexcept>

using namespace std;

int main()
{
    bitset<60> bit1;
    bit1.set(55);

    try
    {

        cout << bit1.to_ulong() << endl;
    }
    catch (overflow_error oerr)
    {
        cout << oerr.what() << endl;
    }
    return 0;
}
