#include <iostream>
#include <vector>

using namespace std;

double sum(vector<double>::const_iterator beg, vector<double>::const_iterator ed)
{
    double sum = 0;
    while((beg++) != ed)
    {
        sum += *beg;
    }
    return sum;
}
int main()
{
    vector<double> vec;
    for (int i = 0; i < 5; ++i)
        vec.push_back(static_cast<double>(i)/2);
    cout << sum(vec.begin(), vec.end()) << endl;
    return 0;
}
