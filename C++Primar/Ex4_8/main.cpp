#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int ary1[] = {1, 2, 3};
    int ary2[] = {1, 2, 3};
    vector<int> vec1;
    vector<int> vec2;
    bool bsame = true;

    for (size_t i = 0; i < 3; ++i)
    {
        vec1.push_back(i);
        vec2.push_back(3-i);
        if(ary1[i] != ary2[i])
            bsame = false;
    }

    if (bsame)
        cout << "same" << endl;
    else
        cout << "different" << endl;

    if (vec1 == vec2)
        cout << "same also" << endl;
    else
        cout << "different also" << endl;

    return 0;
}
