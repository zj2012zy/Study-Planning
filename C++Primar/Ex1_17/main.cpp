#include <iostream>

using namespace std;

int main()
{
    int i, count_ = 0;
    do
    {
        cin >> i;
        if(i < 0)
            count_++;
    }while(i != 0);

    cout << count_ << endl;
    return 0;
}
