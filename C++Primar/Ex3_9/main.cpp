#include <iostream>
#include <string>
#include <cctype>

using namespace std;

int main()
{
    string str;
    cin >> str;
    for(string::size_type i = 0; i != str.size(); )
    {
        if(ispunct(str[i]))
           str.erase(i, 1);
        else
            ++i;
    }

    cout << str << endl;
    return 0;
}
