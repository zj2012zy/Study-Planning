#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    vector<int> vec;
    fill_n(back_inserter(vec), 10, 0);

    for(vector<int>::iterator i = vec.begin(); i != vec.end(); ++i)
        cout << *i << endl;
    return 0;
}
