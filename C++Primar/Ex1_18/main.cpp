#include <iostream>

using namespace std;

int main()
{
    int i, j;
    int low, high;
    cin >> i >> j;
    if( i >= j)
    {
        high = i;
        low = j;
    }
    else
    {
        high = j;
        low = i;
    }
    for(int k = low + 1; k < high; k++)
    {
        cout << k << " ";
        if ((k - (low -1)) % 10 == 0)
            cout << '\n';
    }

    return 0;
}
