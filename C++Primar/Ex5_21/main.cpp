#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> vec;
    for (int i = 0; i < 5; ++i)
    {
        vec.push_back(i);
    }

    vector<int>::iterator i = vec.begin();
    while (i != vec.end())
    {
        (*i % 2 == 1) ? (*i *= 2) : (*i);
        cout << *i++ << endl;
    }
    return 0;
}
