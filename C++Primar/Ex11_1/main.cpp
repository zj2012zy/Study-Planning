#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    vector<int> intvec;
    int i;
    while(cin >> i)
        intvec.push_back(i);

    cout << count(intvec.begin(), intvec.end(), 5) << endl;
    return 0;
}
