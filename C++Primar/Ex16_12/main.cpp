#include <iostream>
#include <map>
#include <vector>
#include <string>

using namespace std;

template <typename IterType>
typename IterType::value_type findMost(IterType beg, IterType ed)
{
    map<typename IterType::value_type, int> mapvalue;
    while(beg != ed)
    {
        if(mapvalue.find(*beg) == mapvalue.end())
            mapvalue.insert(make_pair(*beg, 1));
        else
            mapvalue[*beg] ++;
        beg++;
    }

    typename map<typename IterType::value_type, int>::iterator most = mapvalue.begin();
    for(typename map<typename IterType::value_type, int>::iterator i = mapvalue.begin(); i != mapvalue.end(); ++i)
    {
        if (i->second > most->second)
            most = i;
    }
    return most->first;
}

int main()
{
    vector<string> vecstr;
    vecstr.push_back("love");
    vecstr.push_back("zhaojie");
    vecstr.push_back("zhangying");
    vecstr.push_back("zhangying");
    cout << findMost(vecstr.begin(), vecstr.end()) << endl;
    return 0;
}
