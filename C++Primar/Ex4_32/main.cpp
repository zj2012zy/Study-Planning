#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> vec;
    int arry[5];
    for (int i = 0; i < 5; ++i)
        vec.push_back(i);

    for (vector<int>::size_type i = 0; i < vec.size(); ++i)
        arry[i] = vec[i];

    cout << arry[0] << endl;
    return 0;
}
