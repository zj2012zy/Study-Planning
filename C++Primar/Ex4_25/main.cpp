#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    string s1 = "Small";
    string s2 = "Big";

    const char *c1 = "Small";
    const char *c2 = "Big";

    (s1 >= s2) ? (cout << s2 << endl) : (cout << s1 << endl);
    (strcmp(c1, c2) >=0) ? (cout << s2 << endl) : (cout << s1 << endl);

    return 0;
}
