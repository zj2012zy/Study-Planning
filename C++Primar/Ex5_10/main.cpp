#include <iostream>
#include <bitset>

using namespace std;

int main()
{
    bitset<30> bs;

    bs[27] = 1;
    cout << bs << endl;
    bs[27] = 0;
    cout << bs << endl;

    return 0;
}
