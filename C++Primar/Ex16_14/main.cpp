#include <iostream>
#include <vector>
#include <string>

using namespace std;

template <typename T>
void print(const T& t)
{
    for(typename T::const_iterator i = t.begin(); i != t.end(); ++i)
        cout << *i << endl;
}

int main()
{
    vector<string> vecstr;
    vecstr.push_back("zhaojie");
    vecstr.push_back("love");
    vecstr.push_back("zhangying");
    vecstr.push_back("zhangying");
    print(vecstr);
    return 0;
}
