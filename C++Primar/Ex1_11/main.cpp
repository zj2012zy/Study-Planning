#include <iostream>

using namespace std;

int main()
{
    int j;
    for (int i = 10; i >= 0; i--)
        cout << i << endl;

    j = 10;
    while(j >= 0)
    {
        cout << j << endl;
        j--;
    }
    return 0;
}
