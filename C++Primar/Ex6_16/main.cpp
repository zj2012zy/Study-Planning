#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> vec1;
    vector<int> vec2;
    bool bequal;
    for (int i = 0; i < 3; i++)
        vec1.push_back(i);
    for (int i = 0; i < 5; ++i)
        vec2.push_back(i);

    bequal = true;
    for (vector<int>::iterator i = vec1.begin(), j = vec2.begin(); i != vec1.end(); ++i, ++j)
    {
        if (*i != *j)
        {
            bequal = false;
            break;
        }
    }

    if (bequal)
        cout << "true" << endl;
    else
        cout << "false" << endl;
    return 0;
}
