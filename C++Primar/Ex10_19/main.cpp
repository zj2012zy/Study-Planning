#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <string>
#include <sstream>

using namespace std;

int main()
{
    typedef pair<string, string> child;
    typedef vector<child> children;
    map<string, children> family;
    string group, chi, birthday;

    while(getline(cin, group))
    {
        istringstream line(group);
        line >> group;
        children vecchi;
        while(line >> chi >> birthday)
            vecchi.push_back(make_pair(chi, birthday));
        family.insert(make_pair(group, vecchi));
    }

    return 0;
}
