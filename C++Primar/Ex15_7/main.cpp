#include <iostream>
#include <string>

using namespace std;

class Item_base
{
private:
    string isbn;
protected:
    double price;
public:
    Item_base(const string str = " ", double d = 0):isbn(str), price(d)
    {

    }
    string book() const
    {
        return isbn;
    }
    virtual double net_price(size_t n) const
    {
        return n * price;
    }
    ~Item_base(){}
};

class Bulk_Item: public Item_base
{
protected:
    size_t min_qty;
    double discount;
public:
    double net_price(size_t n) const;
    ~Bulk_Item(){}
};

class Bulk_limit_Item: public Bulk_Item
{
private:
    size_t max_qty;
public:
    double net_price(size_t n) const;
    ~Bulk_limit_Item(){}
};

double Bulk_Item::net_price(size_t n) const
{
    if (n >= min_qty)
        return n * price * discount;
    else
        return n * price;
}

double Bulk_limit_Item::net_price(size_t n) const
{
    if (n >= min_qty)
    {
        if (n >= max_qty)
            return max_qty * price + (n - max_qty) * price *discount;
        else
            return n * price * discount;
    }
    else
        return n * price;
}
int main()
{
    Item_base book1("100-2423-333", 10);
    Bulk_Item book2;
    cout << book1.net_price(5) << endl;
    cout << book2.net_price(5) << endl;
    return 0;
}
