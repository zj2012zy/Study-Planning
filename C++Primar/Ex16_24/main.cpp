#include <iostream>
#include <string>

using namespace std;

template <typename T>
int compare(const T &t1, const T &t2)
{
    if(t1 < t2)
        return 1;
    if(t2 < t1)
        return -1;

    return 0;
}

void func(int (*f)(const string& s1, const string& s2), const string& s1, const string& s2)
{
    cout << f(s1, s2) << endl;
}

void func(int (*f)(const int& i1, const int& i2), int i1, int i2)
{
    cout << f(i1, i2) << endl;
}

int main()
{
    func(compare<string>, "zhaojie", "zhangying");
    func(compare<int>, 1, 2);
    return 0;
}
