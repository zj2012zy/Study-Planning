#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

ifstream & open_file(ifstream &in, const string &file)
{
    in.close();
    in.clear();

    in.open(file.c_str());
    return in;
}
int main()
{
    ifstream in;
    string str;

    open_file(in, "/home/zhaoj/code/C++Primar/Ex8_3/main.cpp") >> str;
    cout << str << endl;
    return 0;
}
