#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;

int main()
{
    vector<int> vecint;
    for(int i = 1022; i < 1033; ++i)
        vecint.push_back(i);

    vector<int>::iterator i = vecint.begin();
    while(i != vecint.end())
    {
        i = find_if(i, vecint.end(), bind2nd(greater<int>(), 1024));
        cout << *i << endl;
        ++i;
    }

    return 0;
}
