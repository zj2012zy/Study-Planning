#include <iostream>
#include <stack>

using namespace std;

int main()
{
    stack<char> exprstack;
    char ch;
    bool bstart = false;

    while(cin >> ch)
    {
        if(ch == '(')
            bstart = true;
        if(ch == ')')
        {
            bstart = false;
            exprstack.push(ch);
        }

        if (bstart)
            exprstack.push(ch);
    }


    while(!exprstack.empty())
    {
        cout << exprstack.top();
        exprstack.pop();
    }

    exprstack.push(10);
    cout << exprstack.top() << endl;
    return 0;
}
