#include <iostream>
#include <string>

using namespace std;

class Screen
{
public:
    typedef string::size_type index;

    Screen(string contents, index height, index width):m_contents(contents), m_height(height), m_width(width)
    {

    }

    char get() const
    {
        return m_contents[m_cursor];
    }

    char get(index r, index c) const;
private:
    string m_contents;
    index m_cursor;
    index m_height, m_width;
};

char Screen::get(index r, index c) const
{
    index row = r * m_width;
    return m_contents[row + c];
}
int main()
{
    Screen screen("I LOVE YOU", 5, 5);
    cout << screen.get(1, 2) << endl;
    return 0;
}
