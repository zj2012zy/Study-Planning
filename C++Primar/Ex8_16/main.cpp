#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

int main()
{
    ifstream fin;
    string str, word;
    istringstream sin;
    vector<string> vec, vec2;

    fin.open("/home/zhaoj/code/C++Primar/Ex8_3/main.cpp");

    while(getline(fin, str))
    {
        vec.push_back(str);
    }
    fin.clear();
    fin.close();

    for(vector<string>::iterator i = vec.begin(); i != vec.end(); ++i)
    {
        sin.str(*i);
        while(sin >> word)
        {
            cout << word << endl;
        }
        sin.clear();
    }

    return 0;
}
