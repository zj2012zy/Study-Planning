#include <iostream>
#include <string>

using namespace std;

int main()
{
    string strpart, strsum;

    cin >> strpart;
    while(strpart.size() != 1)
    {
        strsum += strpart;
        strsum += " ";
        cin >> strpart;
    }
    cout << strsum << endl;
    return 0;
}
