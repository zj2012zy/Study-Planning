#include <iostream>
#include <string>

using namespace std;

class Item_base
{
private:
    string isbn;
protected:
    double price;
public:
    Item_base(const string str = " ", double d = 0):isbn(str), price(d)
    {

    }
    string book() const
    {
        return isbn;
    }
    virtual double net_price(size_t n) const
    {
        return n * price;
    }
    ~Item_base(){}
};

class Bulk_Item: public Item_base
{
private:
    size_t min_qty;
    double discount;
public:
    Bulk_Item(const string &str = " ", double d = 0, size_t mqty = 0, double dis = 0):
        Item_base(str, d), min_qty(mqty), discount(dis)
    {

    }
    double net_price(size_t n) const;
};

double Bulk_Item::net_price(size_t n) const
{
    if (n >= min_qty)
        return n * price * discount;
    else
        return n * price;
}
int main()
{
    Bulk_Item book2("100-2423-333", 10, 3, 0.8);
    Item_base &book1 = book2;
    cout << book1.net_price(5) << endl;
    cout << book2.net_price(5) << endl;
    return 0;
}
