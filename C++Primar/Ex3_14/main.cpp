#include <iostream>
#include <vector>
#include <string>
#include <cctype>

using namespace std;

int main()
{
    vector<string> vecstr;
    string str;
    while(cin >> str)
        vecstr.push_back(str);

    for (vector<string>::size_type i = 0; i != vecstr.size(); ++i)
    {
        if (i % 8 == 0)
            cout << endl;

        for (string::size_type j = 0; j != vecstr[i].size(); ++j)
            vecstr[i][j] = toupper(vecstr[i][j]);

        cout << vecstr[i] << " ";

    }
    return 0;
}
