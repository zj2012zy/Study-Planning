#include <iostream>

using namespace std;

int sum1(int *ary, int mark)
{
    int sum = 0;
    while(*ary++ != mark)
        sum += *ary;
    return sum;
}

int sum2(int *ary1, int *ary2)
{
    int sum = 0;
    while(ary1 != ary2)
    {
        sum += *ary1;
        ++ary1;
    }

    return sum;
}

int sum3(int *ary1, int length)
{
    int sum = 0;
    for (int i = 0; i < length; i++)
        sum += *(ary1 + i);
    return sum;
}
int main()
{
    int aryi[5] = {0, 1, 2, 4, 10};
    cout << sum1(aryi, 10) << endl;
    cout << sum2(aryi, aryi + sizeof(aryi)/sizeof(aryi[0])) << endl;
    cout << sum3(aryi, sizeof(aryi)/sizeof(aryi[0])) << endl;
    return 0;
}
