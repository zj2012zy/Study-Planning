#include <iostream>
#include <string>

using namespace std;

class Person
{
private:
    string m_name;
    string m_address;
public:
    Person(string name, string address):m_name(name), m_address(address)
    {

    }
    string getname() const
    {
        return m_name;
    }

    string getaddress() const
    {
        return m_address;
    }

};

int main()
{
    Person person("zhaojie", "Glodon");
    cout << person.getname() << " " << person.getaddress() << endl;
    return 0;
}
