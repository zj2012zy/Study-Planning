#include <iostream>
#include <vector>

using namespace std;

int main()
{
    const size_t array_size = 10;
    int ary1[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int ary2[10];

    vector<int> vec1;
    vector<int> vec2;

    for (size_t i = 0; i < array_size; ++i)
    {
        ary2[i] = ary1[i];
        vec1.push_back(ary1[i]);
    }

    for (size_t i = 0; i< array_size; ++i)
    {
        cout << ary2[i] << endl;
    }

    vec2 = vec1;
    for (vector<int>::iterator i = vec2.begin(); i != vec2.end(); ++i)
    {
        cout << *i << endl;
    }
    return 0;
}
