#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main()
{
    vector<string*> vecstr;
    string str1 = "hello";
    string str2 = "world";
    string *pstr1, *pstr2;
    pstr1 = &str1;
    pstr2 = &str2;

    vecstr.push_back(pstr1);
    vecstr.push_back(pstr2);

    for(vector<string*>::iterator i = vecstr.begin(); i != vecstr.end(); ++i)
        cout << *(*i) << ((*i)->length()) << endl;
    return 0;
}
