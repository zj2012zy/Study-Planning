#include <iostream>

using namespace std;

int main()
{
    const size_t rowSize = 3;
    const size_t colSize = 4;
    int ia [rowSize][colSize];
    // 12 uninitialized elements
    // for each row
    for (size_t i = 0; i != rowSize; ++i)
    // for each column within the row
        for (size_t j = 0; j != colSize; ++j)
            // initialize to its positional index
            ia[i][j] = i * colSize + j;

    typedef int int_array[4];


    for (int (*p)[4] = &ia[0]; p != ia + 3; ++p)
    {
        for (int *q = *p; q != *p + 4; ++q)
            cout << *q;
        cout << endl;
    }

    return 0;
}
