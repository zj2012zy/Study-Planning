#include <iostream>
#include <string>

using namespace std;

class Item_base
{
private:
    string isbn;
protected:
    double price;
public:
    Item_base(const string str = " ", double d = 0):isbn(str), price(d)
    {

    }
    string book() const
    {
        return isbn;
    }
    virtual double net_price(size_t n) const
    {
        return n * price;
    }
    ~Item_base(){}
};
int main()
{
    Item_base book1("100-2423-333", 10);
    cout << book1.net_price(5) << endl;
    return 0;
}
