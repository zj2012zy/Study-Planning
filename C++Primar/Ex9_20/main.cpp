#include <iostream>
#include <vector>
#include <list>

using namespace std;

int main()
{
    vector<int> vec1, vec2;
    list<int> lis1;
    for(int i = 0; i < 10; ++i)
    {
        vec1.push_back(i);
        lis1.push_back(i);
    }
    lis1.push_back(2);
    vec2.insert(vec2.end(), lis1.begin(), lis1.end());

    if (vec1 == vec2)
        cout << "Same" << endl;
    else
        cout << "different" << endl;
    return 0;
}
