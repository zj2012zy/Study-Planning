#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

int main()
{
    vector<int> intvec;
    int i;
    while(cin >> i)
        intvec.push_back(i);

    cout << accumulate(intvec.begin(), intvec.end(), 0) << endl;
    return 0;
}
