#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> intvec;
    int value;

    while(cin >> value)
        intvec.push_back(value);

    for(vector<int>::size_type i = 0; i != intvec.size(); i++)
    {
        if(i+1 == intvec.size())
            cout << "个数是奇数" << endl;
        else
            cout << intvec[i] + intvec[intvec.size() - 1 - i] << endl;
    }
    return 0;
}
