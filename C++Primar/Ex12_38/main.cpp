#include <iostream>

using namespace std;

class Foo
{
private:
    int  m_i;
public:
    Foo()
    {

    }
    Foo(int i):m_i(i)
    {

    }
    int Geti()
    {
        return m_i;
    }
};

class Bar
{
private:
    static int m_i;
    static Foo m_foo;
public:
    Foo& FooVal()
    {
        return m_foo;
    }
    int CallsFooVal()
    {
        return m_i;
    }
};

int Bar::m_i = 20;
Foo Bar::m_foo(0);

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
