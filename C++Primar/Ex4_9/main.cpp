#include <iostream>

using namespace std;

int main()
{
    const size_t array_size = 10;
    int ary[array_size];

    for (size_t i = 0; i < array_size; ++i)
    {
        ary[i] = i;
    }

    for (size_t i = 0; i < array_size; ++i)
    {
        cout << ary[i] << endl;
    }
    return 0;
}
