#ifndef MYHEADER_INCLUDED
#define MYHEADER_INCLUDED
#include <string>

using namespace std;

inline bool isshorter(const string &str1, const string &str2)
{
    return str1.size() < str2.size();
}


#endif // MYHEADER_INCLUDED
