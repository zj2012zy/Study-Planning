#include "Basket.h"

double Basket::total() const
{
    double sum = 0.0;

    for(const_iter i = items.begin(); i != items.end(); i = items.upper_bound(*i))
    {
        sum += (*i)->net_price(items.count(*i));
    }
    return sum;
}

