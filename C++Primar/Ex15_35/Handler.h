#ifndef HANDLER_H_INCLUDED
#define HANDLER_H_INCLUDED

#include "Items.h"
#include <stdexcept>

class Sales_Item
{
private:
    Item_base *p;
    size_t *use;

    void decr_use()
    {
        if (--*use == 0)
        {
            delete p;
            delete use;
        }
    }
public:
    Sales_Item():p(0), use(new size_t(1))
    {

    }

    Sales_Item(const Item_base&);

    Sales_Item(const Sales_Item& i):p(i.p), use(i.use)
    {
        ++*use;
    }

    ~Sales_Item()
    {
        decr_use();
    }

    Sales_Item& operator=(const Sales_Item&);
    const Item_base *operator->() const
    {
        if (p)
            return p;
        else
            throw logic_error("Unbind Sales_Item");
    }
    const Item_base &operator*() const
    {
        if(p)
            return *p;
        else
            throw logic_error("Unbind Sales_Item");
    }
};

#endif // HANDLER_H_INCLUDED
