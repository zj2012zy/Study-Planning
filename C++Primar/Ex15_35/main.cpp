#include "Items.h"
#include "Basket.h"

int main()
{
    Bulk_Item book2("100-2423-333", 10, 0.8);
    Bulk_Item book5("100-2423-334", 12, 0.7);
    Bulk_Item book6("100-2423-335", 15, 0.9);

    Bulk_limt_Item book3("100-2423-336", 10, 6, 0.6);

    Basket basket;
    basket.add_Item(book2);
    basket.add_Item(book2);
    basket.add_Item(book5);
    basket.add_Item(book6);
    basket.add_Item(book3);
    basket.add_Item(book3);
    basket.add_Item(book3);
    basket.add_Item(book3);
    basket.add_Item(book3);
    basket.add_Item(book3);
    basket.add_Item(book3);

    cout << "The Total is " << basket.total() << endl;
    return 0;
}
