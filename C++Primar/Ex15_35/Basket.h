#ifndef BASKET_H_INCLUDED
#define BASKET_H_INCLUDED

#include "Handler.h"
#include <set>

inline bool compare(const Sales_Item& lhs, const Sales_Item& rhs)
{
    return lhs->book() < rhs->book();
}

class Basket
{
private:
    typedef bool (*Comp)(const Sales_Item& lhs, const Sales_Item& rhs);
public:
    typedef multiset<Sales_Item, Comp> set_type;
    typedef set_type::size_type size_type;
    typedef set_type::const_iterator const_iter;

    Basket(): items(compare) {}
    void add_Item(const Sales_Item& item)
    {
        items.insert(item);
    }
    size_type size(const Sales_Item& i)
    {
        return items.count(i);
    }
    double total() const;
private:
    multiset<Sales_Item, Comp> items;

};
#endif // BASKET_H_INCLUDED
