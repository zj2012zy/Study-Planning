#include "Items.h"

double Bulk_Item::net_price(size_t n) const
{
    return n * price * discount;
}

double Bulk_limt_Item::net_price(size_t n) const
{
    if (n >= min_qty)
        return n * price * discount;
    else
        return n * price;
}
