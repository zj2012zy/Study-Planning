#include "Handler.h"

Sales_Item::Sales_Item(const Item_base& item):p(item.clone()), use(new size_t(1))
{

}

Sales_Item &Sales_Item::operator=(const Sales_Item &rhs)
{
    ++*rhs.use;
    decr_use();
    p = rhs.p;
    use = rhs.use;
    return *this;
}
