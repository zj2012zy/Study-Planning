#ifndef ITEMS_H_INCLUDED
#define ITEMS_H_INCLUDED

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Item_base
{
private:
    string isbn;
protected:
    double price;
public:
    Item_base(const string str = " ", double d = 0):isbn(str), price(d)
    {

    }
    string book() const
    {
        return isbn;
    }
    virtual double net_price(size_t n) const
    {
        return n * price;
    }
    virtual Item_base *clone() const
    {
        return new Item_base(*this);
    }
    virtual ~Item_base()
    {

    }
};

class Disc_Item: public Item_base
{
protected:
    double discount;
public:
    Disc_Item(const string &str = " ", double d = 0, double dis = 0):
        Item_base(str, d),discount(dis)
    {

    }
    double net_price(size_t n) const = 0;
};

class Bulk_Item: public Disc_Item
{
public:
    Bulk_Item(const string &str = " ", double d = 0, double dis = 0):
        Disc_Item(str, d, dis)
    {

    }

    double net_price(size_t n) const;
    Bulk_Item *clone() const
    {
        return new Bulk_Item(*this);
    }
};

class Bulk_limt_Item: public Disc_Item
{
private:
    size_t min_qty;
public:
    Bulk_limt_Item(const string &str = " ", double d = 0, size_t mqty = 0, double dis = 0):
        Disc_Item(str, d, dis), min_qty(mqty)
    {

    }
    double net_price(size_t n) const;

    Bulk_limt_Item *clone() const
    {
        return new Bulk_limt_Item(*this);
    }
};

#endif //ITEMS_H_INCLUDED
