#include <iostream>
#include <string>

using namespace std;

class Employee
{
private:
    int m_ID;
public:
    string m_name;
    Employee()
    {

    }
    Employee(string name):m_name(name)
    {

    }
    Employee(const Employee& emp);
    ~Employee()
    {
        cout << "desctractor" << endl;
    }
    Employee &operator=(const Employee &emp);
    friend ostream& operator<<(ostream& out, const Employee &emp);
};

ostream& operator<<(ostream& out, const Employee &emp)
{
    out << emp.m_name << endl;
    return out;
}
Employee& Employee:: operator=(const Employee &emp)
{
    m_name = emp.m_name;
    m_ID = emp.m_ID;
    return *this;
}

Employee::Employee(const Employee &emp)
{
    m_name = emp.m_name;
    m_ID = emp.m_ID;
}
int main()
{
    Employee emp("zhaojie");
    Employee emp2;
    emp2 = emp;
    cout << emp2.m_name;
    return 0;
}
