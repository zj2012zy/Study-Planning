#include <iostream>

using namespace std;

class ScreenPtr;
class ScrPtr {
    friend class ScreenPtr;
    Screen *sp;
    size_t use;
    ScrPtr(Screen *p): sp(p), use(1) { }
    ~ScrPtr() { delete sp; }
};

class ScreenPtr {
public:
    ScreenPtr(Screen *p): ptr(new ScrPtr(p)) { }
    ScreenPtr(const ScreenPtr &orig):ptr(orig.ptr)
    {
        ++ptr->use;
    }
    ScreenPtr& operator=(const ScreenPtr&);
    ~ScreenPtr() { if (--ptr->use == 0) delete ptr; }
private:
    ScrPtr *ptr;
};

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
