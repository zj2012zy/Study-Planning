#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

template <typename T>
void write_to_stream(ostream &out, T value)
{
    out << value << endl;
}
int main()
{
    string str;
    stringstream sout(str);
    write_to_stream(sout, "zhaojie love you");
    write_to_stream(cout, sout.str());

    write_to_stream(cout, 10);
    write_to_stream(cout, "zhaojie love you");
    ofstream fout("./a.txt");
    write_to_stream(fout, 20);
    write_to_stream(fout, "zhaojie love you");
    return 0;
}
