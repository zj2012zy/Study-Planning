#include <iostream>
#include <string>
#include <vector>
#include <list>

using namespace std;

int main()
{
    vector<string> vecstr;
    list<string> lisstr;
    string str;
    while(cin >> str)
    {
        vecstr.push_back(str);
        lisstr.push_back(str);
    }

    for(vector<string>::iterator i = vecstr.begin(); i != vecstr.end(); ++i)
        cout << *i << endl;
    for(list<string>::iterator i = lisstr.begin(); i != lisstr.end(); ++i)
        cout << *i << endl;
    return 0;
}
