#include <iostream>
#include <list>
#include <deque>

using namespace std;

int main()
{
    list<int> ls;
    deque<int> de1, de2;

    for (int i = 0; i < 10; ++i)
        ls.push_back(i);

    for(list<int>::iterator i = ls.begin(); i != ls.end(); ++i)
    {
        if(*i % 2 == 0)
            de1.push_back(*i);
        else
            de2.push_back(*i);
    }

    for(deque<int>::iterator i = de1.begin(); i != de1.end(); ++i)
        cout << *i << endl;

    for(deque<int>::iterator i = de2.begin(); i != de2.end(); ++i)
        cout << *i << endl;
    return 0;
}
