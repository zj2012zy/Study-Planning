#include <iostream>
#include <vector>

using namespace std;

bool findint(vector<int>::const_iterator first, vector<int>::const_iterator last, int n)
{
    bool bfind = false;
    while(first != last)
    {
        if(*first == n)
        {
            bfind = true;
            break;
        }
        ++first;
    }
    return bfind;
}

vector<int>::iterator &findpos(vector<int>::iterator &first, vector<int>::iterator &last, int n)
{

    while(first != last)
    {
        if(*first == n)
        {
            break;
        }
        ++first;
    }
    return last;
}

int main()
{
    vector<int> vec;

    for(int i = 0; i < 10; i++)
        vec.push_back(i);
    cout << findint(vec.begin(), vec.end(), 11) << endl;
    findpos(vec.begin(), vec.end(), 5));
    return 0;
}
