#include <iostream>

using namespace std;

class Thr
{
public:
    int operator()(int a, int b, int c)
    {
        return a > 0 ? b : c;
    }
};
int main()
{
    Thr thr;
    cout << thr(1, 2, 3) << endl;
    return 0;
}
