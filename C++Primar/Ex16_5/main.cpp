#include <iostream>

using namespace std;

template<typename T>
int Compare(T t1, T t2)
{
    if (t1 == t2)
        return 0;

    if (t1 > t2)
        return 1;

    if (t1 < t2)
        return -1;
}

int main()
{
    cout << Compare("zhaojie", "zhangying") << endl;
    return 0;
}
