#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <algorithm>

using namespace std;

int main()
{
    list<char*> lis;
    vector<string> vec;
    string str;

    lis.push_back("zhaojie");
    lis.push_back("love");
    lis.push_back("zhangying");

    vec.assign(find(lis.begin(), lis.end(), "love"), lis.end());

    for(vector<string>::iterator i = vec.begin(); i != vec.end(); ++i)
        cout << *i << endl;

    cout << "Hello world!" << endl;
    return 0;
}
