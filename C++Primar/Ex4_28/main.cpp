#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> vec1;
    int value;

    while(cin >> value)
        vec1.push_back(value);

    int *p1 = new int[vec1.size()];
    size_t j = 0;
    for (vector<int>::iterator i = vec1.begin(); i != vec1.end(); ++i)
    {
        *(p1 + j) = *i;
        ++j;
    }

    for (size_t k = 0; k < j; ++k)
        cout << *(p1 + k) << endl;

    delete [] p1;
    return 0;
}
