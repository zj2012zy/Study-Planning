#include <iostream>

using namespace std;
#include <iostream>

using namespace std;
class Sale_Item
{
private:
    float units_sold;

    friend ostream &operator<<(ostream &out, const Sale_Item &saleitem);
    friend istream &operator>>(istream &in, Sale_Item &saleitem);
public:
    Sale_Item(float sold = 0):units_sold(sold)
    {

    }

    Sale_Item &operator+=(const Sale_Item &saleitem);
};

Sale_Item &Sale_Item::operator+=(const Sale_Item &saleitem)
{
    units_sold += saleitem.units_sold;
    return *this;
}

Sale_Item operator+(const Sale_Item &saleitem1, const Sale_Item &saleitem2)
{
    Sale_Item ret(saleitem1);
    ret += saleitem2;

    return ret;
}

ostream &operator<<(ostream &out, const Sale_Item& saleitem)
{
    out << saleitem.units_sold;
    return out;
}
int main()
{
    Sale_Item si1(5), si2(2.2);

    cout << si1 + si2 << endl;
    return 0;
}
