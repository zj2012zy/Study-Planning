#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    const size_t default_array_size = 10;
    char *pc = 0;
    char *pc0 = new char[default_array_size];
    char *pcend = pc + default_array_size;
    int i = 0;
    pc = pc0;
    while(cin >> *(pc + i))
    {
        ++i;
        if (pc + i == pcend )
        {
            pc0 = new char[strlen(pc) * 2];
            strcpy(pc0, pc);
            pc = pc0;
        }
    }
    for (size_t j = 0; j <= strlen(pc); ++j)
        cout << *(pc + j);

    delete [] pc;
    return 0;
}
