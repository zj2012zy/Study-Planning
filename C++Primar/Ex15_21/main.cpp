#include <iostream>
#include <string>

using namespace std;

class Item_base
{
private:
    string isbn;
protected:
    double price;
public:
    Item_base(const string str = " ", double d = 0):isbn(str), price(d)
    {

    }
    string book() const
    {
        return isbn;
    }
    virtual double net_price(size_t n) const
    {
        return n * price;
    }
    ~Item_base(){}
};

class Disc_Item: public Item_base
{
protected:
    double discount;
public:
    Disc_Item(const string &str = " ", double d = 0, double dis = 0):
        Item_base(str, d),discount(dis)
    {

    }
    double net_price(size_t n) const;
};

double Disc_Item::net_price(size_t n) const
{
    return n * price * discount;
}

class Bulk_Item: public Disc_Item
{
public:
    Bulk_Item(const string &str = " ", double d = 0, double dis = 0):
        Disc_Item(str, d, dis)
    {

    }
};

class Bulk_limt_Item: public Disc_Item
{
private:
    size_t min_qty;
public:
    Bulk_limt_Item(const string &str = " ", double d = 0, size_t mqty = 0, double dis = 0):
        Disc_Item(str, d, dis), min_qty(mqty)
    {

    }
    double net_price(size_t n) const;
};
double Bulk_limt_Item::net_price(size_t n) const
{
    if (n >= min_qty)
        return n * price * discount;
    else
        return n * price;
}

int main()
{
    Bulk_Item book2("100-2423-333", 10, 0.8);
    Bulk_limt_Item book3("100-2423-333", 10, 6, 0.8);
    Item_base &book1 = book2;
    cout << book1.net_price(5) << endl;
    Item_base &book4 = book3;
    cout << book4.net_price(6) << endl;
    return 0;
}
