#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <vector>
#include "unistd.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    vector<QString> strvec;
    strvec.push_back("I");
    strvec.push_back("Love");
    strvec.push_back("You");

    ui->pushButton->setText("");
    for(vector<QString>::iterator i = strvec.begin(); i != strvec.end(); ++i)
    {
        ui->pushButton->setText(ui->pushButton->text() + *i);
    }
    ui->pushButton->resize(100, 30);
}
